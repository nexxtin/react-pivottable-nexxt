'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Utilities = require('./Utilities');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// helper function for setting row/col-span in pivotTableRenderer
var spanSize = function spanSize(arr, i, j) {
  var x = void 0;
  if (i !== 0) {
    var asc = void 0,
        end = void 0;
    var noDraw = true;
    for (x = 0, end = j, asc = end >= 0; asc ? x <= end : x >= end; asc ? x++ : x--) {
      if (arr[i - 1][x] !== arr[i][x]) {
        noDraw = false;
      }
    }
    if (noDraw) {
      return -1;
    }
  }
  var len = 0;
  while (i + len < arr.length) {
    var asc1 = void 0,
        end1 = void 0;
    var stop = false;
    for (x = 0, end1 = j, asc1 = end1 >= 0; asc1 ? x <= end1 : x >= end1; asc1 ? x++ : x--) {
      if (arr[i][x] !== arr[i + len][x]) {
        stop = true;
      }
    }
    if (stop) {
      break;
    }
    len++;
  }
  return len;
};

function redColorScaleGenerator(values) {
  var min = Math.min.apply(Math, values);
  var max = Math.max.apply(Math, values);
  return function (x) {
    // eslint-disable-next-line no-magic-numbers
    var nonRed = 255 - Math.round(255 * (x - min) / (max - min));
    return { backgroundColor: 'rgb(255,' + nonRed + ',' + nonRed + ')' };
  };
}

var TableRenderer = function (_React$PureComponent) {
  _inherits(TableRenderer, _React$PureComponent);

  function TableRenderer(props) {
    _classCallCheck(this, TableRenderer);

    var _this = _possibleConstructorReturn(this, (TableRenderer.__proto__ || Object.getPrototypeOf(TableRenderer)).call(this, props));

    _this.state = {
      inputTableData: _this.initiateDataTable(props)
      // tableSaved: false,
    };

    return _this;
  }

  _createClass(TableRenderer, [{
    key: 'initiateDataTable',
    value: function initiateDataTable(props) {
      var dataTableArray = [];

      var pivotData = new _Utilities.PivotData(props);
      pivotData.rowKeys.map(function (row, r_index) {
        dataTableArray.push([]);
        if (pivotData.colKeys.length == 0) {
          dataTableArray[r_index].push(0);
        } else {
          pivotData.colKeys.map(function (col) {
            dataTableArray[r_index].push(0);
          });
        }
      });

      // console.log(dataTableArray);

      return dataTableArray;
    }
  }, {
    key: 'componentWillReceiveProps',


    // TODO: allow parent component to pass a stored data table down
    value: function componentWillReceiveProps(newProps) {
      if (this.props.colAttributes.length != newProps.colAttributes.length || this.props.rowAttributes.length != newProps.rowAttributes.length) {
        console.log("Table Renderer Update Alter!");
        // console.log("", this.state.inputTableData);
        // if (this.arrayComparer(this.props.colAttributes, newProps.cols) || this.arrayComparer(this.props.rowAttributes, newProps.rows)) {
        this.setState({
          inputTableData: this.initiateDataTable(newProps)
        });
      }
    }
  }, {
    key: 'arrayComparer',
    value: function arrayComparer(arr1, arr2) {
      var compareResult = false;
      arr1.map(function (item1) {
        arr2.map(function (item2) {
          if (item1 != item2) {
            compareResult = true;
          }
        });
      });

      return compareResult;
    }
  }, {
    key: 'getRowSum',
    value: function getRowSum(row) {
      var rowSum = 0;
      this.state.inputTableData[row].map(function (rowNum) {
        rowSum += rowNum;
      });
      return rowSum;
    }
  }, {
    key: 'getColSum',
    value: function getColSum(col) {
      var colSum = 0;
      this.state.inputTableData.map(function (rowArr) {
        colSum += rowArr[col];
      });
      return colSum;
    }
  }, {
    key: 'getTotalSum',
    value: function getTotalSum() {
      var totalSum = 0;
      this.state.inputTableData.map(function (rowArr) {
        rowArr.map(function (elem) {
          totalSum += elem;
        });
      });
      return totalSum;
    }
  }, {
    key: 'handleSave',
    value: function handleSave() {
      if (this.state.inputTableData) {
        this.props.updateQuotaTable(this.state.inputTableData);
      }

      this.props.hideUISideBars();

      // this.setState({
      //     tableSaved: true,
      // });
    }
  }, {
    key: 'handleEdit',
    value: function handleEdit() {
      if (!this.props.saveQuota) {
        this.props.showUISideBars();
      }

      // this.setState({
      //     tableSaved: true,
      // });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var pivotData = new _Utilities.PivotData(this.props);
      var colAttrs = this.props.colAttributes ? this.props.colAttributes : pivotData.props.cols;
      var rowAttrs = this.props.rowAttributes ? this.props.rowAttributes : pivotData.props.rows;
      var rowKeys = pivotData.getRowKeys();
      var colKeys = pivotData.getColKeys();
      var grandTotalAggregator = pivotData.getAggregator([], []);

      var handleTableInputChange = function handleTableInputChange(event, i, j) {
        _this2.setState({
          inputTableData: Array.from(_this2.state.inputTableData, function (subarray, row) {
            return Array.from(subarray, function (elem, col) {
              return row == i && col == j ? Number(event.target.value) : elem;
            });
          })
        });
      };

      var valueCellColors = function valueCellColors() {};
      var rowTotalColors = function rowTotalColors() {};
      var colTotalColors = function colTotalColors() {};
      var getClickHandler = this.props.tableOptions && this.props.tableOptions.clickCallback ? function (value, rowValues, colValues) {
        var filters = {};
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = Object.keys(colAttrs || {})[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var i = _step.value;

            var attr = colAttrs[i];
            if (colValues[i] !== null) {
              filters[attr] = colValues[i];
            }
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator.return) {
              _iterator.return();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }

        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
          for (var _iterator2 = Object.keys(rowAttrs || {})[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
            var _i = _step2.value;

            var attr = rowAttrs[_i];
            if (rowValues[_i] !== null) {
              filters[attr] = rowValues[_i];
            }
          }
        } catch (err) {
          _didIteratorError2 = true;
          _iteratorError2 = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion2 && _iterator2.return) {
              _iterator2.return();
            }
          } finally {
            if (_didIteratorError2) {
              throw _iteratorError2;
            }
          }
        }

        return function (e) {
          return _this2.props.tableOptions.clickCallback(e, value, filters, pivotData);
        };
      } : null;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'table',
          { className: 'pvtTable' },
          _react2.default.createElement(
            'thead',
            null,
            colAttrs.map(function (c, j) {
              return _react2.default.createElement(
                'tr',
                { key: 'colAttr' + j },
                j === 0 && rowAttrs.length !== 0 && _react2.default.createElement('th', { colSpan: rowAttrs.length, rowSpan: colAttrs.length }),
                _react2.default.createElement(
                  'th',
                  { className: 'pvtAxisLabel' },
                  c
                ),
                colKeys.map(function (colKey, i) {
                  var x = spanSize(colKeys, i, j);
                  if (x === -1) {
                    return null;
                  }
                  return _react2.default.createElement(
                    'th',
                    {
                      className: 'pvtColLabel',
                      key: 'colKey' + i,
                      colSpan: x,
                      rowSpan: j === colAttrs.length - 1 && rowAttrs.length !== 0 ? 2 : 1
                    },
                    colKey[j]
                  );
                }),
                j === 0 && _react2.default.createElement(
                  'th',
                  {
                    className: 'pvtTotalLabel',
                    rowSpan: colAttrs.length + (rowAttrs.length === 0 ? 0 : 1)
                  },
                  'Totals'
                )
              );
            }),
            rowAttrs.length !== 0 && _react2.default.createElement(
              'tr',
              null,
              rowAttrs.map(function (r, i) {
                return _react2.default.createElement(
                  'th',
                  { className: 'pvtAxisLabel', key: 'rowAttr' + i },
                  r
                );
              }),
              _react2.default.createElement(
                'th',
                { className: 'pvtTotalLabel' },
                colAttrs.length === 0 ? 'Totals' : null
              )
            )
          ),
          _react2.default.createElement(
            'tbody',
            null,
            rowKeys.map(function (rowKey, i) {
              var totalAggregator = pivotData.getAggregator(rowKey, []);
              return _react2.default.createElement(
                'tr',
                { key: 'rowKeyRow' + i },
                rowKey.map(function (txt, j) {
                  var x = spanSize(rowKeys, i, j);
                  if (x === -1) {
                    return null;
                  }
                  return _react2.default.createElement(
                    'th',
                    {
                      key: 'rowKeyLabel' + i + '-' + j,
                      className: 'pvtRowLabel',
                      rowSpan: x,
                      colSpan: j === rowAttrs.length - 1 && colAttrs.length !== 0 ? 2 : 1
                    },
                    txt
                  );
                }),
                colKeys.map(function (colKey, j) {
                  var aggregator = pivotData.getAggregator(rowKey, colKey);
                  return _react2.default.createElement(
                    'td',
                    {
                      className: 'pvtVal',
                      key: 'pvtVal' + i + '-' + j
                    },
                    _this2.props.hideSideBars ? _react2.default.createElement(
                      'div',
                      null,
                      _this2.state.inputTableData[i][j]
                    ) : _react2.default.createElement('input', {
                      type: 'number',
                      style: { width: "60px" },
                      value: _this2.state.inputTableData[i][j],
                      onChange: function onChange(e) {
                        return handleTableInputChange(e, i, j);
                      }
                    })
                  );
                }),
                _react2.default.createElement(
                  'td',
                  {
                    className: 'pvtTotal',
                    onClick: getClickHandler && getClickHandler(totalAggregator.value(), rowKey, [null]),
                    style: colTotalColors(totalAggregator.value())
                  },
                  _this2.props.hideSideBars ? _this2.state.inputTableData[i][0] : _this2.props.noCols ? _react2.default.createElement('input', {
                    type: 'number',
                    style: { width: "60px" },
                    value: _this2.state.inputTableData[i][0],
                    onChange: function onChange(e) {
                      return handleTableInputChange(e, i, 0);
                    }
                  }) : _this2.getRowSum(i)
                )
              );
            }),
            _react2.default.createElement(
              'tr',
              null,
              _react2.default.createElement(
                'th',
                {
                  className: 'pvtTotalLabel',
                  colSpan: rowAttrs.length + (colAttrs.length === 0 ? 0 : 1)
                },
                'Totals'
              ),
              colKeys.map(function (colKey, i) {
                var totalAggregator = pivotData.getAggregator([], colKey);
                return _react2.default.createElement(
                  'td',
                  {
                    className: 'pvtTotal',
                    key: 'total' + i,
                    onClick: getClickHandler && getClickHandler(totalAggregator.value(), [null], colKey),
                    style: rowTotalColors(totalAggregator.value())
                  },
                  _this2.getColSum(i)
                );
              }),
              _react2.default.createElement(
                'td',
                {
                  onClick: getClickHandler && getClickHandler(grandTotalAggregator.value(), [null], [null]),
                  className: 'pvtGrandTotal'
                },
                this.getTotalSum()
              )
            )
          )
        ),
        this.props.hideSideBars ? _react2.default.createElement(
          'button',
          { onClick: this.handleEdit.bind(this) },
          'Edit Table'
        ) : _react2.default.createElement(
          'button',
          { onClick: this.handleSave.bind(this) },
          'Save Table'
        )
      );
    }
  }]);

  return TableRenderer;
}(_react2.default.PureComponent);

TableRenderer.defaultProps = _Utilities.PivotData.defaultProps;
TableRenderer.propTypes = _Utilities.PivotData.propTypes;
TableRenderer.defaultProps.tableColorScaleGenerator = redColorScaleGenerator;
TableRenderer.defaultProps.tableOptions = {};
TableRenderer.propTypes.tableColorScaleGenerator = _propTypes2.default.func;
TableRenderer.propTypes.tableOptions = _propTypes2.default.object;

var TSVExportRenderer = function (_React$PureComponent2) {
  _inherits(TSVExportRenderer, _React$PureComponent2);

  function TSVExportRenderer() {
    _classCallCheck(this, TSVExportRenderer);

    return _possibleConstructorReturn(this, (TSVExportRenderer.__proto__ || Object.getPrototypeOf(TSVExportRenderer)).apply(this, arguments));
  }

  _createClass(TSVExportRenderer, [{
    key: 'render',
    value: function render() {
      var pivotData = new _Utilities.PivotData(this.props);
      var rowKeys = pivotData.getRowKeys();
      var colKeys = pivotData.getColKeys();
      if (rowKeys.length === 0) {
        rowKeys.push([]);
      }
      if (colKeys.length === 0) {
        colKeys.push([]);
      }

      var headerRow = pivotData.props.rows.map(function (r) {
        return r;
      });
      if (colKeys.length === 1 && colKeys[0].length === 0) {
        headerRow.push(this.props.aggregatorName);
      } else {
        colKeys.map(function (c) {
          return headerRow.push(c.join('-'));
        });
      }

      var result = rowKeys.map(function (r) {
        var row = r.map(function (x) {
          return x;
        });
        colKeys.map(function (c) {
          var v = pivotData.getAggregator(r, c).value();
          row.push(v ? v : '');
        });
        return row;
      });

      result.unshift(headerRow);

      return _react2.default.createElement('textarea', {
        value: result.map(function (r) {
          return r.join('\t');
        }).join('\n'),
        style: { width: window.innerWidth / 2, height: window.innerHeight / 2 },
        readOnly: true
      });
    }
  }]);

  return TSVExportRenderer;
}(_react2.default.PureComponent);

TSVExportRenderer.defaultProps = _Utilities.PivotData.defaultProps;
TSVExportRenderer.propTypes = _Utilities.PivotData.propTypes;

exports.default = {
  Table: TableRenderer
};
module.exports = exports['default'];
//# sourceMappingURL=TableRenderers.js.map