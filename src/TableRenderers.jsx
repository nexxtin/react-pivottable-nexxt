import React from 'react';
import PropTypes from 'prop-types';
import {PivotData} from './Utilities';

// helper function for setting row/col-span in pivotTableRenderer
const spanSize = function(arr, i, j) {
  let x;
  if (i !== 0) {
    let asc, end;
    let noDraw = true;
    for (
      x = 0, end = j, asc = end >= 0;
      asc ? x <= end : x >= end;
      asc ? x++ : x--
    ) {
      if (arr[i - 1][x] !== arr[i][x]) {
        noDraw = false;
      }
    }
    if (noDraw) {
      return -1;
    }
  }
  let len = 0;
  while (i + len < arr.length) {
    let asc1, end1;
    let stop = false;
    for (
      x = 0, end1 = j, asc1 = end1 >= 0;
      asc1 ? x <= end1 : x >= end1;
      asc1 ? x++ : x--
    ) {
      if (arr[i][x] !== arr[i + len][x]) {
        stop = true;
      }
    }
    if (stop) {
      break;
    }
    len++;
  }
  return len;
};

function redColorScaleGenerator(values) {
  const min = Math.min.apply(Math, values);
  const max = Math.max.apply(Math, values);
  return x => {
    // eslint-disable-next-line no-magic-numbers
    const nonRed = 255 - Math.round(255 * (x - min) / (max - min));
    return {backgroundColor: `rgb(255,${nonRed},${nonRed})`};
  };
}

  class TableRenderer extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            inputTableData: this.initiateDataTable(props),
            // tableSaved: false,
        };

    };

    initiateDataTable(props) {
        var dataTableArray = [];

        const pivotData = new PivotData(props);
        pivotData.rowKeys.map((row, r_index) => {
            dataTableArray.push([]);
            if (pivotData.colKeys.length == 0) {
                dataTableArray[r_index].push(0);
            } else {
                pivotData.colKeys.map(col => {
                    dataTableArray[r_index].push(0);
                });
            }
        });

        // console.log(dataTableArray);

        return dataTableArray;
    };

    // TODO: allow parent component to pass a stored data table down
    componentWillReceiveProps(newProps) {
        if (this.props.colAttributes.length != newProps.colAttributes.length ||
            this.props.rowAttributes.length != newProps.rowAttributes.length) {
            console.log("Table Renderer Update Alter!");
            // console.log("", this.state.inputTableData);
        // if (this.arrayComparer(this.props.colAttributes, newProps.cols) || this.arrayComparer(this.props.rowAttributes, newProps.rows)) {
            this.setState({
                inputTableData: this.initiateDataTable(newProps),
            });
        }
    };

    arrayComparer(arr1, arr2) {
        var compareResult = false;
        arr1.map(item1 => {
            arr2.map(item2 =>{
                if (item1 != item2) {
                    compareResult = true;
                }
            });
        });

        return compareResult;
    };

    getRowSum(row) {
        var rowSum = 0;
        this.state.inputTableData[row].map(rowNum => {rowSum += rowNum});
        return rowSum;
    };

    getColSum(col) {
        var colSum = 0;
        this.state.inputTableData.map(rowArr => {colSum += rowArr[col]});
        return colSum;
    };

    getTotalSum() {
        var totalSum = 0;
        this.state.inputTableData.map(rowArr =>
            {rowArr.map(elem => {totalSum += elem})}
        );
        return totalSum;
    };

    handleSave() {
        if (this.state.inputTableData) {
            this.props.updateQuotaTable(this.state.inputTableData);
        }

        this.props.hideUISideBars();

        // this.setState({
        //     tableSaved: true,
        // });
    };

    handleEdit() {
        if (!this.props.saveQuota) {
            this.props.showUISideBars();
        }

        // this.setState({
        //     tableSaved: true,
        // });
    };

    render() {
      const pivotData = new PivotData(this.props);
      const colAttrs = this.props.colAttributes ? this.props.colAttributes : pivotData.props.cols;
      const rowAttrs = this.props.rowAttributes ? this.props.rowAttributes : pivotData.props.rows;
      const rowKeys = pivotData.getRowKeys();
      const colKeys = pivotData.getColKeys();
      const grandTotalAggregator = pivotData.getAggregator([], []);

      const handleTableInputChange = (event, i, j) => {
          this.setState({
              inputTableData: Array.from(this.state.inputTableData, (subarray, row) =>
                Array.from(subarray, (elem, col) => {
                    return (row == i && col == j) ? Number(event.target.value) : elem;
                }))
          });
      };

      let valueCellColors = () => {};
      let rowTotalColors = () => {};
      let colTotalColors = () => {};
      const getClickHandler =
        this.props.tableOptions && this.props.tableOptions.clickCallback
          ? (value, rowValues, colValues) => {
              const filters = {};
              for (const i of Object.keys(colAttrs || {})) {
                const attr = colAttrs[i];
                if (colValues[i] !== null) {
                  filters[attr] = colValues[i];
                }
              }
              for (const i of Object.keys(rowAttrs || {})) {
                const attr = rowAttrs[i];
                if (rowValues[i] !== null) {
                  filters[attr] = rowValues[i];
                }
              }
              return e =>
                this.props.tableOptions.clickCallback(
                  e,
                  value,
                  filters,
                  pivotData
                );
            }
          : null;

      return (
        <div>
            <table className="pvtTable">
              <thead>
                {colAttrs.map(function(c, j) {
                  return (
                    <tr key={`colAttr${j}`}>
                      {j === 0 &&
                        rowAttrs.length !== 0 && (
                          <th colSpan={rowAttrs.length} rowSpan={colAttrs.length} />
                        )}
                      <th className="pvtAxisLabel">{c}</th>
                      {colKeys.map(function(colKey, i) {
                        const x = spanSize(colKeys, i, j);
                        if (x === -1) {
                          return null;
                        }
                        return (
                          <th
                            className="pvtColLabel"
                            key={`colKey${i}`}
                            colSpan={x}
                            rowSpan={
                              j === colAttrs.length - 1 && rowAttrs.length !== 0
                                ? 2
                                : 1
                            }
                          >
                            {colKey[j]}
                          </th>
                        );
                      })}

                      {j === 0 && (
                        <th
                          className="pvtTotalLabel"
                          rowSpan={
                            colAttrs.length + (rowAttrs.length === 0 ? 0 : 1)
                          }
                        >
                          Totals
                        </th>
                      )}
                    </tr>
                  );
                })}

                {rowAttrs.length !== 0 && (
                  <tr>
                    {rowAttrs.map(function(r, i) {
                      return (
                        <th className="pvtAxisLabel" key={`rowAttr${i}`}>
                          {r}
                        </th>
                      );
                    })}
                    <th className="pvtTotalLabel">
                      {colAttrs.length === 0 ? 'Totals' : null}
                    </th>
                  </tr>
                )}
              </thead>

              <tbody>
                {rowKeys.map((rowKey, i) => {
                  const totalAggregator = pivotData.getAggregator(rowKey, []);
                  return (
                    <tr key={`rowKeyRow${i}`}>
                      {rowKey.map(function(txt, j) {
                        const x = spanSize(rowKeys, i, j);
                        if (x === -1) {
                          return null;
                        }
                        return (
                          <th
                            key={`rowKeyLabel${i}-${j}`}
                            className="pvtRowLabel"
                            rowSpan={x}
                            colSpan={
                              j === rowAttrs.length - 1 && colAttrs.length !== 0
                                ? 2
                                : 1
                            }
                          >
                            {txt}
                          </th>
                        );
                      })}
                      {colKeys.map((colKey, j) => {
                        const aggregator = pivotData.getAggregator(rowKey, colKey);
                        return (
                            <td
                                className="pvtVal"
                                key={`pvtVal${i}-${j}`}
                            >
                                {this.props.hideSideBars ? (
                                    <div>
                                        {this.state.inputTableData[i][j]}
                                    </div>
                                ) : (
                                    <input
                                        type="number"
                                        style={{width:"60px"}}
                                        value={this.state.inputTableData[i][j]}
                                        onChange={e => handleTableInputChange(e, i , j)}
                                      />
                                )}
                            </td>
                        );
                      })}
                      <td
                        className="pvtTotal"
                        onClick={
                          getClickHandler &&
                          getClickHandler(totalAggregator.value(), rowKey, [null])
                        }
                        style={colTotalColors(totalAggregator.value())}
                      >
                        {this.props.hideSideBars ? (
                            this.state.inputTableData[i][0]
                        ) : (
                            this.props.noCols ? (
                                <input
                                    type="number"
                                    style={{width:"60px"}}
                                    value={ this.state.inputTableData[i][0]}
                                    onChange={e => handleTableInputChange(e, i , 0)}
                                  />
                            ) : (this.getRowSum(i))
                        )}
                      </td>
                    </tr>
                  );
                })}

                <tr>
                  <th
                    className="pvtTotalLabel"
                    colSpan={rowAttrs.length + (colAttrs.length === 0 ? 0 : 1)}
                  >
                    Totals
                  </th>

                  {colKeys.map((colKey, i) => {
                    const totalAggregator = pivotData.getAggregator([], colKey);
                    return (
                      <td
                        className="pvtTotal"
                        key={`total${i}`}
                        onClick={
                          getClickHandler &&
                          getClickHandler(totalAggregator.value(), [null], colKey)
                        }
                        style={rowTotalColors(totalAggregator.value())}
                      >
                        {this.getColSum(i)}
                      </td>
                    );
                  })}

                  <td
                    onClick={
                      getClickHandler &&
                      getClickHandler(grandTotalAggregator.value(), [null], [null])
                    }
                    className="pvtGrandTotal"
                  >
                    {this.getTotalSum()}
                  </td>
                </tr>
              </tbody>
            </table>

            {this.props.hideSideBars ? (
                <button onClick={this.handleEdit.bind(this)}>Edit Table</button>
            ) : (
                <button onClick={this.handleSave.bind(this)}>Save Table</button>
            )}
        </div>
      );
    }
  }

  TableRenderer.defaultProps = PivotData.defaultProps;
  TableRenderer.propTypes = PivotData.propTypes;
  TableRenderer.defaultProps.tableColorScaleGenerator = redColorScaleGenerator;
  TableRenderer.defaultProps.tableOptions = {};
  TableRenderer.propTypes.tableColorScaleGenerator = PropTypes.func;
  TableRenderer.propTypes.tableOptions = PropTypes.object;

class TSVExportRenderer extends React.PureComponent {
  render() {
    const pivotData = new PivotData(this.props);
    const rowKeys = pivotData.getRowKeys();
    const colKeys = pivotData.getColKeys();
    if (rowKeys.length === 0) {
      rowKeys.push([]);
    }
    if (colKeys.length === 0) {
      colKeys.push([]);
    }

    const headerRow = pivotData.props.rows.map(r => r);
    if (colKeys.length === 1 && colKeys[0].length === 0) {
      headerRow.push(this.props.aggregatorName);
    } else {
      colKeys.map(c => headerRow.push(c.join('-')));
    }

    const result = rowKeys.map(r => {
      const row = r.map(x => x);
      colKeys.map(c => {
        const v = pivotData.getAggregator(r, c).value();
        row.push(v ? v : '');
      });
      return row;
    });

    result.unshift(headerRow);

    return (
      <textarea
        value={result.map(r => r.join('\t')).join('\n')}
        style={{width: window.innerWidth / 2, height: window.innerHeight / 2}}
        readOnly={true}
      />
    );
  }
}

TSVExportRenderer.defaultProps = PivotData.defaultProps;
TSVExportRenderer.propTypes = PivotData.propTypes;

export default {
  Table: TableRenderer,
};
